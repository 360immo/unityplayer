﻿using System.Collections;
using System;
using UnityEngine;
using Models;
using VRInteraction;

public class LinkEvent : MonoBehaviour {

    Link _link;   

    Color m_MouseOverColor = Color.red;
    Color m_OriginalColor;
    SpriteRenderer m_Renderer;

    public event Action<LinkEvent> OnButtonSelected;                   // This event is triggered when the selection of the button has finished.

    [SerializeField] private SelectionRadial m_SelectionRadial;         // This controls when the selection is complete.
    [SerializeField] private VRInteractiveItem m_InteractiveItem;       // The interactive item for where the user should click to load the level.


    private bool m_GazeOver;                                            // Whether the user is looking at the VRInteractiveItem currently.

    void Awake()
    {
        m_Renderer = GetComponent<SpriteRenderer>();
        m_OriginalColor = m_Renderer.material.color;
        m_InteractiveItem = GetComponent<VRInteractiveItem>();
        m_SelectionRadial = GameObject.Find("CameraXform").GetComponent<SelectionRadial>();
    }

    void OnMouseOver() 
    {
        m_Renderer.material.color = m_MouseOverColor;
        if (Input.GetMouseButtonDown(0))
        {
            SceneManager.s_manager.ChangePhoto(_link.idNextPhoto);
        }
    }

    void OnMouseExit()
    {
        m_Renderer.material.color = m_OriginalColor;
    }

    public void Set_link(Link link)
    {
        _link = link;
    }

    private void OnEnable()
    {
        m_InteractiveItem.OnOver += HandleOver;
        m_InteractiveItem.OnOut += HandleOut;
        m_SelectionRadial.OnSelectionComplete += HandleSelectionComplete;
    }


    private void OnDisable()
    {
        m_InteractiveItem.OnOver -= HandleOver;
        m_InteractiveItem.OnOut -= HandleOut;
        m_SelectionRadial.OnSelectionComplete -= HandleSelectionComplete;
    }


    private void HandleOver()
    {
        // When the user looks at the rendering of the scene, show the radial.
        m_SelectionRadial.Show();

        m_GazeOver = true;
    }


    private void HandleOut()
    {
        // When the user looks away from the rendering of the scene, hide the radial.
        m_SelectionRadial.Hide();

        m_GazeOver = false;
    }


    private void HandleSelectionComplete()
    {
        // If the user is looking at the rendering of the scene when the radial's selection finishes, activate the button.
        if (m_GazeOver)
            ActivateButton();
        m_SelectionRadial.Hide();
        m_GazeOver = false;
    }


    private void ActivateButton()
    {
        // change the photo.
        SceneManager.s_manager.ChangePhoto(_link.idNextPhoto);
    }
}
