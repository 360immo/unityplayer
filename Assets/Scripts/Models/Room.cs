﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Models
{
    [System.Serializable]
    public struct Room
    {
        public string name;
        public string description;
        public string surface;

        public int nombrePhoto;
        public List<Photo> Photos;
    }
}
