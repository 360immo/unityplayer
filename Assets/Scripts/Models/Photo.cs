﻿using System.Collections.Generic;

namespace Models
{
    [System.Serializable]
    public class Photo {

        public int id;

        public string name;
        public string description;
        public string path;

        public bool autoRotation;
        public float direction;

        public List<Link> Links;
        [System.NonSerialized] public List<Tooltip> Tools;

        [System.NonSerialized] public Room room;

        public Photo(string path, string name, string description)
        {
            this.path = path;
            this.name = name;
            this.description = description;
        }
    }
}
