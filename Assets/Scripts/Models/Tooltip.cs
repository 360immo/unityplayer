﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Models
{
    public class Tooltip
    {

        public Vector3 _rotation;
        public Photo _currentPhoto;
        public string _name;
        public string _description;

        public Tooltip(Vector3 rot, Photo photo, string name, string description)
        {
            _rotation = rot;
            _currentPhoto = photo;
            _name = name;
            _description = description;
        }
    }
}