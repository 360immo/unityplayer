﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR;

public class SceneLoader : MonoBehaviour {

    private GameObject _currentCamera;

    [SerializeField]
    private GameObject _WindowsCamera;
    [SerializeField]
    private GameObject _AndroidVRCamera;

    void Awake()
    {
        if (Application.platform == RuntimePlatform.Android)
        {

        } else
        {
            _currentCamera = _WindowsCamera;
        }
    }

    // Use this for initialization
    void Start () {
        GameObject myGameObject = Object.Instantiate(_currentCamera, Vector3.zero, Quaternion.identity);

        myGameObject.name = "CameraContainer";
    }
	
	// Update is called once per frame
	void Update () {
        //If V is pressed, toggle VRSettings.enabled
        Debug.Log("Changed VRSettings.enabled to:" + UnityEngine.XR.XRSettings.enabled);
        if (Input.GetKeyDown(KeyCode.V))
        {
            Debug.Log("Changed VRSettings.enabled to:" + UnityEngine.XR.XRSettings.enabled);
            UnityEngine.XR.XRSettings.enabled = true;
            Debug.Log("Changed VRSettings.enabled to:" + UnityEngine.XR.XRSettings.enabled);
        }
    }
}
