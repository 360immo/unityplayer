﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using Models;

public class SceneManager : MonoBehaviour {

    public static SceneManager s_manager;

    private string _path;
    private Photo _current;

    private VisiteData _data;
    private Hashtable _t_Photos;

    private GameObject _sphereViewer;
    [SerializeField]
    private LinkEvent prefab;

    private List<GameObject> _l_linkPrefabs;

    /*
     * Unity Functions
     */

    void Awake()
    {
        _sphereViewer = GameObject.Find("SphereView");
        _l_linkPrefabs = new List<GameObject>();
    }

	// Use this for initialization
	void Start () {
        if (s_manager == null)
            s_manager = this;

        //Get static value set in main menue
        var visite = EngineData.VisitPath;

        //IF in editor
        if (Application.isEditor)
        {
            visite = openVisitFolder(visite); // Only work on editor _ to change by the visit download
        }
        
        //Read json file
        StreamReader sr = new StreamReader(visite);
        string json = sr.ReadToEnd();
        sr.Close();

        //Parse Json
        _data = JsonUtility.FromJson<VisiteData>(json);

        //Format Photo data for the engine
        _t_Photos = new Hashtable();
        foreach (var room in _data.Rooms)
        {
            foreach (var photo in room.Photos)
            {
                photo.room = room;
                _t_Photos[photo.id] = photo;
                photo.path = photo.path.Replace('\\', '/');
            }
        }

        //Set the first photo
        ChangePhoto(_data.first);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    /*
     * Public functions 
     */

    public void ChangePhoto(int id)
    {
        var photo = (Photo)_t_Photos[id];
        if (photo == null)
        {
            Debug.Log("Photo Id not found");
        }

        //Create the C# path to load the photo texture
        var photoPath = "file://" + _path + photo.path;

        //Create the object texture that will be apply to the sphereviewer shader
        Texture2D tex;
        tex = new Texture2D(512, 512, TextureFormat.ARGB32, true);
        //Load the photo
        WWW www = new WWW(photoPath);
        www.LoadImageIntoTexture(tex);
        //Apply the texture
        _sphereViewer.GetComponent<Renderer>().material.mainTexture = tex;

        _current = photo;
        //Change the north of the photo
        GameObject.Find("sphereViewMesh").transform.eulerAngles = new Vector3(0,_current.direction,0);

        //Destroy previous links
        foreach (var linkPrefab in _l_linkPrefabs)
        {
            Destroy(linkPrefab);
        }

        _l_linkPrefabs.Clear();

        //Generate new links
        foreach (var link in photo.Links)
        {
            var linkPrefab = createLink(link);
            _l_linkPrefabs.Add(linkPrefab);
        }

        
        updateHUD();
    }

    public string Get_path()
    {
        return _path;
    }

    /*
     * Private functions
     */
    
    /*
     * If in editor and no Visit Path Data
     * This function open file loader of unity editor to select a visit
     */ 
    private string openVisitFolder(string visite)
    {
        if (visite == null)
            _path = EditorUtility.OpenFolderPanel("Load visit folders", "", "");
        else
            _path = visite;
        string[] files = Directory.GetFiles(_path);

        foreach (string file in files)
            if (file.EndsWith(".json"))
                return file;

        return null;
    }

    public GameObject createLink(Link link)
    {
        var v_normal = new Vector2(0, 1);
        var v_dir = new Vector2(link.x, link.y);

        GameObject newLink = Instantiate(prefab.gameObject, new Vector3(0, 0, -2.3f), Quaternion.identity) as GameObject;
     //   Debug.Log("rotation = " + rotation + " | V_dir = " + v_dir);
        newLink.transform.RotateAround(this.transform.position, Vector3.up, link.orientation);
        LinkEvent linkPrefab = newLink.GetComponent<LinkEvent>();
        linkPrefab.Set_link(link);

        return newLink;
    }

    public void updateHUD()
    {
        var panelParent = GameObject.Find("InformationPanel");

        GameObject.Find("PhotoName").GetComponent<UnityEngine.UI.Text>().text = _current.name;
        GameObject.Find("RoomName").GetComponent<UnityEngine.UI.Text>().text = _current.room.name;
        GameObject.Find("PhotoDescription").GetComponent<UnityEngine.UI.Text>().text = _current.description;
        GameObject.Find("Description").GetComponent<UnityEngine.UI.Text>().text = _current.room.description;
        GameObject.Find("Surface").GetComponent<UnityEngine.UI.Text>().text = _current.room.surface;
    }
}
