﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LeftMenuScript : MonoBehaviour {

    //reference for the open and close button
    public Button openButton;
    public Button closeButton;

    //refrence for the left panel in the hierarchy
    public GameObject leftPanel;
    //animator reference
    private Animator anim;

    // Use this for initialization
    void Start () {
        //get the animator component
        anim = leftPanel.GetComponent<Animator>();
        //disable it on start to stop it from playing the default animation
        anim.enabled = false;
    }

    // Update is called once per frame
    void Update () {
		
	}

    //function to Open the Panel
    public void OpenPanel()
    {
        //enable the animator component
        anim.enabled = true;
        //play the Slidein animation
        anim.Play("LeftPanelOpen");

        openButton.gameObject.SetActive(false);
        closeButton.gameObject.SetActive(true);
    }
    //function to Close the Panel
    public void ClosePanel()
    {
        //play the SlideOut animation
        anim.Play("LeftPanelClose");

        openButton.gameObject.SetActive(true);
        closeButton.gameObject.SetActive(false);
    }

    public void ToggleAutoRotate()
    {
        var obj = GameObject.Find("CameraContainer");
        if (obj == null)
            return;

        CameraController camera = (CameraController)obj.GetComponent(typeof(CameraController));
        if (camera != null)
           camera.ToggleAutoRotate();
    }


}
