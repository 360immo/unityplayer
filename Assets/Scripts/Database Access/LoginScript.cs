﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.IO;
using System;
using System.Net;
using System.Web;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using Models;
using UnityEditor;
using System.Xml.Serialization;

public class Visit
{

    [XmlAttribute("ID")]
    public int ID;
    [XmlAttribute("View")]
    public int View;
    [XmlAttribute("VisitName")]
    public string VisitName;
    [XmlAttribute("File")]
    public string File;
    [XmlAttribute("Path")]
    public string Path;

    public Visit() { }

    public Visit(int identifier, int viewing, string name, string export)
    {
        this.ID = identifier;
        this.View = viewing;
        this.VisitName = name;
        this.File = export;
    }
}

[XmlRoot("PersistentData")]
public class PersistentData
{
    [XmlArray("Visits")]
    [XmlArrayItem("Visit")]
    public List<Visit> Visits;
    [XmlAttribute("savePath")]
    public string savePath;

    public PersistentData() { }
}

public class User
{
    public string CompanyName;
    public string Mail;
    public List<Visit> Visits;
    public int ID;

    public User(string company, string email, int identifier)
    {
        this.CompanyName = company;
        this.Mail = email;
        this.ID = identifier;
    }
}

public class LoginScript : MonoBehaviour
{
    public InputField InputLogin;
    public InputField InputPassword;
    public Text display;

    public static User s_loggedInUser = null;
    public static PersistentData s_visits;
    // Use this for initialization
    void Start()
    {
        InputLogin = GameObject.Find("InputLogin").GetComponent<InputField>();
        InputPassword = GameObject.Find("InputPassword").GetComponent<InputField>();
        display = GameObject.Find("ErrorText").GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Login()
    {
        if (LoginScript.s_visits.savePath == null)
        {
            LoginScript.s_visits.savePath = EditorUtility.OpenFolderPanel("Select visit storage folder", "", "") + "/";
        }

        if (retrieveVisitsJSON())
        {
            Application.LoadLevel("ListVisits");
        }
    }

    public bool retrieveVisitsJSON()
    {
        string connectionString = "Server=163.172.63.192;Port=3360;Database=360IMMO;Uid=UserSoftware;Pwd=B9tv9Q9cBxMjrUGc";

        MySqlConnection connect = null;
        MySqlDataReader rdr = null;
        try
        {
            int count = 0;
            //Attempt user connection
            connect = new MySqlConnection(connectionString);
            connect.Open();
            MySqlCommand command = new MySqlCommand(null, connect);

            command.CommandText = "SELECT id,email,password,companyName FROM Users WHERE email = @mail AND password = MD5(SHA1(MD5(@pass)))";
            Debug.Log(InputLogin.ToString());
            Debug.Log(InputLogin.text);
            command.Parameters.AddWithValue("@mail", InputLogin.text);
            command.Parameters.AddWithValue("@pass", InputPassword.text);

            command.Prepare();
            rdr = command.ExecuteReader(); //Execute Query
            while (rdr.Read())
            {
                LoginScript.s_loggedInUser = new User(rdr.GetString(3), rdr.GetString(1), rdr.GetInt32(0));
                count += 1;
            }
            if (count < 1)
            {
                display.text = "Wrong mail address or password. Please try again.";
                return (false);
            }
            else
            {
                connect.Close();
                count = 0;
                display.text = "Successfully logged in.";
                //Load all visits for the user
                connect = new MySqlConnection(connectionString);
                connect.Open();
                command = new MySqlCommand(null, connect);
                command.CommandText = "SELECT id,id_Users,View,Name,File,lastEdit FROM Visit WHERE id_Users = @id";
                command.Parameters.AddWithValue("@id", LoginScript.s_loggedInUser.ID);

                command.Prepare();
                rdr = command.ExecuteReader();
                LoginScript.s_loggedInUser.Visits = new List<Visit>();
                Visit tmp;
                //For each visit retrieved
                display.text = "Loading... Please wait.";
                while (rdr.Read())
                {
                    //Create a new visit
                    tmp = new Visit(rdr.GetInt32(0), rdr.GetInt32(2), rdr.GetString(3), rdr.GetString(4));
                    //Create a directory with the visit name and a Visit.json inside
                    tmp.Path = createVisitOnDisk(tmp);
                    //Add the visit to the list if it doesn't already exist
                    bool add = true;
                    foreach (Visit actual in LoginScript.s_visits.Visits)
                    {
                        if (tmp.Path == actual.Path)
                        {
                            add = false;
                            break;
                        }
                    }
                    if (add)
                    {
                        LoginScript.s_visits.Visits.Add(tmp);
                    }
                    count += 1;
                }
                if (count < 1)
                {
                    display.text = "Visit retrieval failed. Please try again.";
                    return (false);
                }
                savePrefsToFile();
                display.text = "Visit retrieval success.";
                return (true);
            }
        }
        catch (MySqlException ex)
        {
            display.text = ex.ToString();
            return (false);
        }
        finally
        {
            if (connect != null)
            {
                connect.Close();
            }
        }
    }

    void savePrefsToFile()
    {
        string path = AppDomain.CurrentDomain.BaseDirectory + "preferences.360IMMOPREFS";
        var serializer = new XmlSerializer(typeof(PersistentData));

        var memoryStream = new MemoryStream();
        var streamWriter = new StreamWriter(memoryStream, System.Text.Encoding.UTF8);

        serializer.Serialize(streamWriter, LoginScript.s_visits);
        byte[] utf8EncodedXml = memoryStream.ToArray();

        var stream = new FileStream(path, FileMode.Create);
        stream.Write(utf8EncodedXml, 0, utf8EncodedXml.Length);
        memoryStream.Close();
        stream.Close();
        /*string json = "{\"savePath\": \"" + LoginScript.s_visits.savePath + "\",\n\"Visits\": [";
        int i = 1;
        foreach (Visit tmp in LoginScript.s_visits.Visits)
        {
            json += JsonUtility.ToJson(LoginScript.s_visits.Visits.ToArray()[0]);
            if (i != LoginScript.s_visits.Visits.Count)
                json += ",\n";
            i += 1;
        }
        json += "]}";
        json += JsonUtility.ToJson(LoginScript.s_visits.visitNames);// "}";
        string path = AppDomain.CurrentDomain.BaseDirectory + "preferences.360IMMOPREFS";
        File.WriteAllText(path, json);*/
    }

    string createVisitOnDisk(Visit toCreate)
    {
        //Create a directory with the visit name and a Visit.json inside
        string path = LoginScript.s_visits.savePath + toCreate.VisitName;
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
            StreamWriter sw = File.CreateText(path + "/Visit.json");
            sw.WriteLine(toCreate.File);
            Directory.CreateDirectory(path + "/Photos");
            downloadFilesFTP(JsonUtility.FromJson<VisiteData>(toCreate.File), path + "/Photos/");
        }
        return path;
    }

    void downloadFilesFTP(VisiteData data, string localpath)
    {
        foreach(Room tmpRoom in data.Rooms)
        {
            foreach (Photo tmpPhoto in tmpRoom.Photos)
            {
              using (WebClient request = new WebClient())
                {
                    request.Credentials = new NetworkCredential("ftpuser", "727FsMCRWjZzNy5P");
                    string url = ("ftp://163.172.63.192:21/visit" + tmpPhoto.path.Replace("\\", "/").Replace(" ", ""));
                    Debug.Log(url);
                    byte[] fileData = request.DownloadData(url);
                    Debug.Log("download");
                    using (FileStream file = File.Create(localpath + tmpPhoto.path.Substring(tmpPhoto.path.LastIndexOf("\\") + 1)))
                    {
                        file.Write(fileData, 0, fileData.Length);
                        file.Close();
                    }
                }
            }
        }
    }
}