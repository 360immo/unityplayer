﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.IO;
using System;
using System.Net;
using System.Web;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using Models;
using UnityEditor;
using System.Xml.Serialization;

public class DisplayVisitsScript : MonoBehaviour
{

    public Text info;
    public Image visits;
    public GameObject prefab;

    // Use this for initialization
    void Start()
    {
        visits = GameObject.Find("ListScroll").GetComponent<Image>();
        prefab = GameObject.Find("VisitInstance");
        DisplayVisits();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void DisplayVisits()
    {
        if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + "preferences.360IMMOPREFS"))
        {
            StreamReader sr = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + "preferences.360IMMOPREFS");
            //string json = sr.ReadToEnd();
            //LoginScript.s_visits = JsonUtility.FromJson<PersistentData>(json);
            XmlSerializer serializer = new XmlSerializer(typeof(PersistentData));
            LoginScript.s_visits = serializer.Deserialize(sr) as PersistentData;
            sr.Close();
        }
        else
        {
            LoginScript.s_visits = new PersistentData();
        }
        if (LoginScript.s_visits.Visits == null)
        {
            LoginScript.s_visits.Visits = new List<Visit>();
        }
        List<Visit> toDisplay = LoginScript.s_visits.Visits;
        int count = 1;
        foreach (Visit tmp in toDisplay)
        {
            if (Directory.Exists(tmp.Path))
            {
                if (count == 1)
                {
                    prefab.GetComponentInChildren<Text>().text = tmp.VisitName;
                    prefab.GetComponent<Button>().name = tmp.Path;
                }
                else
                {
                    GameObject button = (GameObject)Instantiate(prefab);
                    button.GetComponent<Button>().name = tmp.Path;

                    var panel = GameObject.Find("ListScroll");
                    button.transform.position = panel.transform.position;
                    button.GetComponent<RectTransform>().SetParent(panel.transform);
                    button.GetComponentInChildren<Text>().text = tmp.VisitName;
                }
                count += 1;
            }
        }
        Debug.Log("count " + count);
        if (count <= 1)
        {
            prefab.SetActive(false);
        }
    }

    public void Download()
    {
        //Visit download
        LoginScript.s_loggedInUser = null;
        Application.LoadLevel("MainMenu");
    }

    public void LoadVisit()
    {
        Debug.Log(EventSystem.current.currentSelectedGameObject.name);
        Debug.Log(EventSystem.current.currentSelectedGameObject.GetComponent<Button>().name);
        EngineData.VisitPath = EventSystem.current.currentSelectedGameObject.GetComponent<Button>().name; //"C:/Users/Lucas/IdeaProjects/unityplayer2/Assets/Visites/warmupEpitech";
        Application.LoadLevel("photoViewerDestock");
    }
}
