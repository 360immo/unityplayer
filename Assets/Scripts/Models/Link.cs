﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Models
{
    [System.Serializable]
    public class Link
    {
        public float x;
        public float y;

        public string direction;

        public int idNextRoom;
        public int idNextPhoto;

        public string pathNextPhoto;

        public float orientation;

        [System.NonSerialized] public float _rotation;
    }
}
