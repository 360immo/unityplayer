﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    [SerializeField]
    private float mousseRotateSpeed = 100.0f;
    [SerializeField]
    private float keyRotateSpeed = 70.0f;
    [SerializeField]
    private float autoRotateSpeed = 30.0f;

    private bool autoRotate = false;


    // Use this for initialization
    void Start () {
	}
	
    private void RotateCamera(float rotateAboutX, float rotateAboutY)
    {
        gameObject.transform.Rotate(rotateAboutX, rotateAboutY, 0.0f); // Apply Rotate

        // Lock z rotation to 0 so camera doesn't 'roll' sideways.
        var newRotation = gameObject.transform.rotation.eulerAngles;
        newRotation.z = 0;
        gameObject.transform.rotation = Quaternion.Euler(newRotation);
    }

    public void ToggleAutoRotate()
    {
        if (autoRotate)
            autoRotate = false;
        else
            autoRotate = true;
    }

    // Update is called once per frame
    void Update () {

        if (Input.GetMouseButton(0)) //left click
        {
            autoRotate = false;

            float rotateAboutX, rotateAboutY;

            rotateAboutX = Input.GetAxis("Mouse Y") * Time.deltaTime * mousseRotateSpeed;
            rotateAboutY = -Input.GetAxis("Mouse X") * Time.deltaTime * mousseRotateSpeed;

            RotateCamera(rotateAboutX, rotateAboutY);
        }
        else if (Input.GetAxis("Vertical") + Input.GetAxis("Horizontal") != 0)
        {
            autoRotate = false;
            float rotateAboutX = 0.0f, rotateAboutY = 0.0f;

            rotateAboutX = Input.GetAxis("Vertical") * Time.deltaTime * keyRotateSpeed;
            rotateAboutY = Input.GetAxis("Horizontal") * Time.deltaTime * keyRotateSpeed;

            RotateCamera(rotateAboutX, rotateAboutY);
        }

        if (autoRotate)
        {
            RotateCamera(0.0f, Time.deltaTime * autoRotateSpeed);
        }
    }
}
