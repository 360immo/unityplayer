﻿public static class EngineData
{
    private static string visitpath = null;

    public static string VisitPath
    {
        get
        {
            return visitpath;
        }
        set
        {
            visitpath = value;
        }
    }
}
