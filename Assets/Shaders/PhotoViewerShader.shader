﻿// Upgrade NOTE: upgraded instancing buffer 'Props' to new syntax.

Shader "Custom/PhotoViewerShader" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		//This is used to print the texture inside of the sphere 
		Cull Front

		CGPROGRAM

#pragma surface surf SimpleLambert
		half4 LightingSimpleLambert(SurfaceOutput s, half3 lightDir, half atten)
		{
			half4 c;
			c.rgb = s.Albedo;
			c.a = s.Alpha;
			return c;
		}

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
			float4 myColor : COLOR;
		};

		fixed3 _Color;

		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutput o) {
			//This is used to mirror the image correctly when printing it inside of the sphere
			IN.uv_MainTex.x = 1 - IN.uv_MainTex.x;
			fixed3 result = tex2D(_MainTex, IN.uv_MainTex)*_Color;
			o.Albedo = result.rgb;
			o.Alpha = 1;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
