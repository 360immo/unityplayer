﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Models
{
    [System.Serializable]
    public class VisiteData
    {
        public string name;
        public string date;
        public string auteur;

        public int nombrePhoto;
        public int first;

        public List<Room> Rooms;
    }
}